import { Component, OnInit } from '@angular/core';
import { YaEvent } from "angular8-yandex-maps";
import { HttpService } from 'src/app/services/http.service'

class Point {
  public feature: any;
  public options: any;

  constructor(public lat: number, public long: number, public radius: number) {
    this._setFeature();
    this._setOptions();
  }

  private _setFeature(): void {
    this.feature = {
      geometry: {
        type: "Circle",
        draggable: true,
        coordinates: [this.lat, this.long],
        radius: this.radius,
        startEditing: true
      },
    };
  }
  // Rectangle, Circle
  // .editor.startEditing();

  private _setOptions(): void {
    this.options = {
      draggable: true
    };
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  param = false
  public points: Point[] = [];

  radius = 1000

  search_value = ""

  stop_script = false // Эта переменная скрывает круг, если был получен результат

  json = []


  constructor(
    private httpService: HttpService
  ) { }

  public ngOnInit(): void {
    console.log(new Point(55.159892, 61.402541, this.radius))
    this.points.push(new Point(55.159892, 61.402541, this.radius));
  }

  public onClick(event: YaEvent): void {
    if (this.stop_script) {
      return
    }
    const coords = event.event.get("coords");
    // this.points.push(new Point(coords[0], coords[1], 1000));
    this.points[0] = new Point(coords[0], coords[1], this.radius)
    console.log(this.points[0])
    // console.log(event.event.get("coords"))
  }

  add_new_radius() {
    for (let i = 0; i < this.points.length; i++) {
      this.points[0] = new Point(this.points[0].lat, this.points[0].long, this.radius)
    }
  }

  search_this_text_value() {
    if (this.search_value == '') {
      return console.log('Введите текст')
    }
    // https://search-maps.yandex.ru/v1/
    // ? apikey=<ключ>
    // & text=<поисковый запрос>
    // & lang=<язык ответа>
    // & [type=<типы объектов>]
    // & [ll=<центр области поиска>]
    // & [spn=<размеры области поиска>]
    // & [bbox=<координаты области поиска>]
    // & [rspn=<не искать за пределами области поиска>]
    // & [results=<количество результатов в ответе>]
    // & [skip=<количество пропускаемых результатов>]
    // & [callback=<имя функции>]

    let lat = this.points[0].lat - (this.points[0].lat - (this.points[0].radius / 10000))
    let long = this.points[0].long - (this.points[0].long - (this.points[0].radius / 10000))

    let ll = this.points[0].long + ',' + this.points[0].lat
    let spn = long + ',' + lat

    const url = 'https://search-maps.yandex.ru/v1/?text=' + this.search_value + '&type=biz&ll=' + ll + '&spn=' + spn + '&lang=ru_RU&apikey=b35707bb-c280-463a-a6ae-12f95821f06d';
    // console.log('url: ', url)
    this.httpService.get(url, {}).subscribe(async data => {
      console.log('ответ яндекса: ', data)

      let array_data = []
      if (data['features'].length == 0) {
        return
      }
      for (let i = 0; i < data['features'].length; i++) {
        try {
          console.log('Объект такой: ', data['features'][i])

          let ara = data['features'][i].geometry.coordinates[0]
          data['features'][i].geometry.coordinates[0] = data['features'][i].geometry.coordinates[1]
          data['features'][i].geometry.coordinates[1] = ara
          let phone = ''
          if (data['features'][i].properties.CompanyMetaData.Phones != undefined) {
            phone = '<h7>Телефон: ' + data['features'][i].properties.CompanyMetaData.Phones[0].formatted + '</h7>'
          }

          data['features'][i].properties.hintContent = '<div class="map__hint">' + data['features'][i].properties.description + '</div>',
            data['features'][i].properties.balloonContent = [
              '<div class="container"><div class="row"><div class="col-12"><h5>Название: "' + data['features'][i].properties.name + '"</h5><h7>Деятельность: <b>' + data['features'][i].properties.CompanyMetaData.Categories[0].name + '</b></h7><br><h7>Адрес: ' + data['features'][i].properties.description + '</h7><br>' + phone + '</div></div></div>',
            ]

          array_data[i] = { feature: data['features'][i] }
          this.json[i] = data['features'][i]
        } catch (e) {
          console.log(e)
        }
      }

      this.points = array_data

      // < ----------------- ВНИМАНИЕ ----------------- > //
      // Здесь нужно отправить Илье array_data
      // < ----------------- ВНИМАНИЕ ----------------- > //

      // От Ильи мы получаем ['id','id','id','id',]

      // Когда получаем ответ, вызываем это:
      // this.parser_json(array_data, ['Сюда вставить ответ от ИЛЬИ'])

    })

  }

  // Сюда присылаем ответ от ИЛЬИ
  parser_json(json, array) {
    // Здесь мы должны перебрать полученный array и отстортировать из JSON лишнее.

    let data_comp = [] // Сюда кладём теневые компании
    console.log('Количество компаний: ', json.length)


    for (let i = 0; i < json.length; i++) {
      // for (let j = 0; j < array.length; j++) {
      //   if (json[i].properties.CompanyMetaData.id == array[j]) {
      console.log('json[i].properties: ', json[i].properties)
      json[i].properties.hintContent = '<div class="map__hint">' + json[i].properties.description + '</div>',
        json[i].properties.balloonContent = [
          '<div class="container"><div class="row"><div class="col-12"><h5>Название: "' + json[i].properties.name + '"</h5><h7>Деятельность: <b>' + json[i].properties.CompanyMetaData.Categories[0].name + '</b></h7><br><h7>Адрес: ' + json[i].properties.description + '</h7><br><h7>Телефон: ' + json[i].properties.CompanyMetaData.Phones[0].formatted + '</h7></div></div></div>',
        ]

      data_comp.push({ feature: json[i] }) // записываем результат и это позже выведем на карту.
      //   }
      // }

    }
    console.log('Результат работы программы: ', data_comp)
    this.stop_script = true // НЕ забыть вернуть false, если будем делать повторный поиск!
    this.points = data_comp

  }

  changeRadiusParam = 1000
  changeRadius(name: any) {
    if (name == 'up') {
      this.radius = this.radius + 1000
    }
    if (name == 'down') {
      this.radius = this.radius - 1000
    }
    this.points[0] = new Point(this.points[0].lat, this.points[0].long, this.radius)
  }

  showResult = false
  toggleSearchCase() {
    this.showResult = !this.showResult
  }

  toggleCheckCase() {
    this.param = !this.param
  }

  dataSetSession() {
    localStorage.setItem('resultSearch', 'lol')
  }

  dataGetSession() {
    return JSON.parse(localStorage.getItem('resultSearch') || '[]')
  }
}
