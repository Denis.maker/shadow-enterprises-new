import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  headers = new HttpHeaders()
  options = { headers: this.headers, withCredintials: false }

  constructor(private http: HttpClient) {}

  post(serviceName: string, data: any) {
    const url = serviceName
    return this.http.post(url, data, this.options)
  }

  get(url, data) {
    return this.http.get(url, data)
  }
}
